# PDF Editor
![halleyx_screen_recorder](/uploads/54e039455242ab0367d50c21fc27a4c7/halleyx_screen_recorder.mp4)
  

## How to use pdf-editor?

1. Click `Choose PDF` to upload a `.pdf` file.
2. Add images, signatures, text to your PDF.
3. Click `Save`.
4. That's it! All is done **in your browser**.

## Features

- Resize and move everything.
- Add signatures.
- Adjust line height, font size, font family.
- Mobile friendly.
- Drag and drop to upload your PDF.



